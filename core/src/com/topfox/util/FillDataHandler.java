package com.topfox.util;

import com.topfox.common.DataDTO;
import com.topfox.data.Field;

import java.util.List;
import java.util.Map;

public interface FillDataHandler {
    void fillData(Map<String,Field> fillFields, List<DataDTO> list);
}
