# 1. 目录
- [快速使用](https://gitee.com/topfox/topfox/blob/dev/guide/explain-sample.md)
- [常见问题](https://gitee.com/topfox/topfox/blob/dev/guide/explain-question.md)
- [TopFox配置参数](https://gitee.com/topfox/topfox/blob/dev/guide/explain-configuration.md)
- [上下文对象](https://gitee.com/topfox/topfox/blob/dev/guide/explain-tools.md)
- [核心使用](https://gitee.com/topfox/topfox/blob/dev/guide/explain-core.md)
- [条件匹配器](https://gitee.com/topfox/topfox/blob/dev/guide/explain-condition.md)
- [实体查询构造器](https://gitee.com/topfox/topfox/blob/dev/guide/explain-entityselect.md)
- [流水号生成器](https://gitee.com/topfox/topfox/blob/dev/guide/explain-keybuild.md)
- [数据校验组件](https://gitee.com/topfox/topfox/blob/dev/guide/explain-checkdata.md)
- [更新日志组件](https://gitee.com/topfox/topfox/blob/dev/guide/explain-updatelog.md)
- [Response 返回结果对象](https://gitee.com/topfox/topfox/blob/dev/guide/explain-response.md)

## 1.1. 必备
- 文中涉及的例子源码网址: https://gitee.com/topfox/topfox-sample
- TopFox技术交流群 QQ: 874732179

## 1.2. topfox 介绍
* 基于 srpingboot2.x.x, mybaties
* 无侵入, 轻损耗
* 支持主键自动生成, 支持多主键查询、修改等
* 内置全局、局部拦截插件：提供delete、update 自定义拦截功能
* 支持devtools/jrebel热部署
* 支持在不使用devtools/jrebel的情况下, 热加载 mybatis的mapper文件
* 自带Redis缓存功能, 支持多主键模式, 自定义redis-key
* 有相关代码生成器, 只需少量修改即可生成即用代码
* 拥有预防Sql注入攻击功能

## 1.3. topfox 使用
> 推荐上传到自有的maven-nexus服, 然后在业务项目进行以下依赖

```
<!--topfox-->
<dependency>
    <groupId>com.topfox</groupId>
    <artifactId>topfox</artifactId>
    <version>1.1.0</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

# 2. 更新日志 
## 2.1. 版本1.2.1  更新日志
请参考 << TopFox配置参数 >>中的描述, 增加配置3个参数  :

- top.service.update-not-result-error
- top.service.select-by-before-update
- top.service.redis-log

dto增加 addNullfields 方法, 请参考 <<常见问题>> 章节中的 "如何将指定字段的值更新为null"


## 2.2. 版本1.2.0  更新日志

- updateBatch 方法的返回值 由int 改为 List< DTO >
- BaseDao 作废删除, 增加   BaseMapper< DTO extends DataDTO > 代替

- AdvancedService< QTO extends DataQTO, DTO extends DataDTO >  改为
<BR> AdvancedService< M extends BaseMapper< DTO >, DTO extends DataDTO >

- SimpleService 中 方法 setBaseDao 删除.  已经定义 baseMapper , 可访问开发者自定义Dao的方法

## 2.3. 版本1.1.0  更新日志
### 2.3.1. 上下文对象
- 在以后的版本中将删除 com.topfox.common.ApplicationContextProvider 
- 新增加 上下文对象 com.topfox.common.AppContaxt,静态方法如下:

```
	getBean(Class<T> clazz)
	getBean(String name,Class<T> clazz)
	getRestSessionHandler()
	getAbstractRestSession() 
	getSysConfig()
	environment()
```

### 2.3.2. SimpleService 进行了改造和优化
新增方法

```
List<DTO> selectBatch(DataQTO qto)
Response<List<DTO>> selectPage(EntitySelect entitySelect)
Response<List<Map<String, Object>>> selectPageForMap(EntitySelect entitySelect)
List<DTO> selectBatch(DataQTO qto)
Response<List<DTO>> listPage(EntitySelect entitySelect)
List<DTO> listObjects(EntitySelect entitySelect)
```

### 2.3.3. EntitySelect 和DataQTO
EntitySelect 和 DataQTO 对 orderBy groupBy having 分页的支持  

### 2.3.4. ResponseCode 增加了多个 异常编码 保证 Topxfox抛出异常 的编码唯一性
